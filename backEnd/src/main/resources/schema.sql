
create table person
(
   id integer auto_increment,
   name varchar(255) not null,
   birth_date date not null,
   primary_document_number varchar(255) not null,
   gender varchar(255) not null,
   address varchar(255),
   primary key(id),
);

