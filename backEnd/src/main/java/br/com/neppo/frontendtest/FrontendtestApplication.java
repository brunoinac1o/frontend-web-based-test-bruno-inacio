package br.com.neppo.frontendtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FrontendtestApplication {

	public static void main(String[] args) {
		SpringApplication.run(FrontendtestApplication.class, args);
	}
}
