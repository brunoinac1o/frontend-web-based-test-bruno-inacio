package br.com.neppo.frontendtest.model;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by brunoi on 18/04/18.
 */
@SuppressWarnings("Since15")
public class PersonRowMapper implements RowMapper< Person >  {

    @Override
    public Person mapRow(ResultSet rs, int rowNum) throws SQLException {
        Person person = new Person();
        person.setId(rs.getInt("id"));
        person.setName(rs.getString("name"));
        person.setBirthDate( rs.getDate("birth_date"));
        person.setPrimaryDocumentNumber( rs.getString("primary_document_number") );
        person.setGender( rs.getString("gender") );
        person.setAddress( rs.getString( "address" ) );
        return person;
    }
}

