package br.com.neppo.frontendtest.repository;

import br.com.neppo.frontendtest.model.Person;
import br.com.neppo.frontendtest.model.PersonRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

/**
 * Created by brunoi on 18/04/18.
 */
@Repository
public class PersonRepository {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<Person> all() {
        return jdbcTemplate.query("select * from person", new PersonRowMapper());
    }

    public Person findById( Integer id ) {
        return jdbcTemplate.queryForObject("select * from person where id=?", new Object[] {
                    id
                },
                new BeanPropertyRowMapper< Person >(Person.class));
    }

    public Integer remove( Integer id ) {
        return jdbcTemplate.update("delete from person where id=?", new Object[] {
                id
        });

    }

    public Integer save(final Person person) {

        final String INSERT_SQL = "insert into person ( name, birth_date, primary_document_number, gender, address) values(?,  ?, ?, ?, ? )";

        final java.sql.Timestamp sq = new java.sql.Timestamp(person.getBirthDate().getTime());


        KeyHolder key = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {

            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                final PreparedStatement ps = connection.prepareStatement(INSERT_SQL,
                        Statement.RETURN_GENERATED_KEYS);
                ps.setString(1, String.valueOf(person.getName()));
                ps.setTimestamp(2, sq);
                ps.setString(3, String.valueOf(person.getPrimaryDocumentNumber()));
                ps.setString(4, String.valueOf(person.getGender()));
                ps.setString( 5, String.valueOf( person.getAddress() ) );
                return ps;
            }
        }, key);

        return (Integer) key.getKey();

    }


    public int update(Person person) {
        return jdbcTemplate.update("update person " + " set name = ?, birth_date = ?, primary_document_number = ?, gender =?, address =?  " + " where id = ?",
                new Object[] {
                        person.getName(), person.getBirthDate(), person.getPrimaryDocumentNumber(), person.getGender(), person.getAddress(), person.getId()
                });
    }

    public Integer getQuantityByGender( String gender ){

        int count = jdbcTemplate.queryForObject(
                "select count(*) from person where gender=?", new Object[] { gender }, Integer.class);

        return count;
    }

    public Integer getQuantityByAge( Integer initialAge, Integer finalAge ){

        int count = jdbcTemplate.queryForObject(
                "select count(*) from person where Year( birth_date  ) >? and  Year( birth_date  ) <?", new Object[] { initialAge,  finalAge }, Integer.class);

        return count;
    }


}

