package br.com.neppo.frontendtest.model;

import br.com.neppo.frontendtest.utils.CustomerDateAndTimeDeserialize;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Date;


/**
 * Created by brunoi on 18/04/18.
 */

@Entity
public class Person {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;

    @JsonDeserialize(using=CustomerDateAndTimeDeserialize.class)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private Date birthDate;

    private String primaryDocumentNumber;

    private String gender;

    private String address;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getPrimaryDocumentNumber() {
        return primaryDocumentNumber;
    }

    public void setPrimaryDocumentNumber(String primaryDocumentNumber) {
        this.primaryDocumentNumber = primaryDocumentNumber;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", birthDate='" + birthDate + '\'' +
                ", primaryDocumentNumber='" + primaryDocumentNumber + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }


}
