package br.com.neppo.frontendtest.service;

import br.com.neppo.frontendtest.model.Person;
import br.com.neppo.frontendtest.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.Calendar;
import java.util.List;

/**
 * Created by brunoi on 18/04/18.
 */
@Service
public class PersonService {

    @Autowired
    private PersonRepository personRepository;

    public List<Person> list() {
        return personRepository.all();
    }

    public Person findById(Integer id ) {

        Assert.notNull( id, "Id não pode ser nulo" );

        return personRepository.findById( id );

    }


    public Person delete(Integer id) {

        Assert.notNull( id, "Id não pode ser nulo" );

        personRepository.remove(id);

        return null;
    }

    public void save(Person person) {

        validatePerson(person);

        personRepository.save(person);
    }

    public void update(Person person) {

        Assert.notNull( person, "person não pode ser nulo" );

        Assert.notNull( person.getId() , "Id não pode ser nulo" );

        validatePerson(person);

        personRepository.update(person);
    }

    public Integer getQuantityByGender( String gender ) {

        Assert.notNull( gender, "sexo não pode ser nulo" );

        return personRepository.getQuantityByGender(gender);

    }

    public Integer getQuantityByAge( Integer initialAge, Integer finalAge ) {

        Assert.notNull( initialAge, "Idade inicial não pode ser nulo" );

        Assert.notNull( finalAge, "Idade Final não pode ser nulo" );

        int year = Calendar.getInstance().get(Calendar.YEAR );

        return personRepository.getQuantityByAge( year - initialAge, year -finalAge);

    }


    private void validatePerson(Person person ){

        Assert.notNull( person, "person não pode ser nulo" );

        Assert.notNull( person.getPrimaryDocumentNumber(), "Documento não pode ser nulo" );

        Assert.notNull( person.getName(), "Nome não pode ser nulo" );

        Assert.notNull( person.getBirthDate(), "Data de nascimento não pode ser nulo" );

        Assert.notNull( person.getGender(), "Sexo não pode ser nulo" );


    }

}
