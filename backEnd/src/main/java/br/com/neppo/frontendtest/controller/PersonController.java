package br.com.neppo.frontendtest.controller;

import br.com.neppo.frontendtest.model.Person;
import br.com.neppo.frontendtest.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by brunoi on 18/04/18.
 */

@Controller
@RequestMapping("/Person")
public class PersonController {

    @Autowired
    private PersonService personService;


    @ResponseBody
    @GetMapping("/")
    public ResponseEntity<List<Person>> list() {
        try {
            return new ResponseEntity<List<Person>>(personService.list(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<List<Person>>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @ResponseBody
    @GetMapping("/{id}")
    public ResponseEntity<Person> findById(@PathVariable("id") Integer id) {
        try {
            return new ResponseEntity<Person>(personService.findById(id), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Person>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    @DeleteMapping("/{id}")
    public ResponseEntity<Person> delete(@PathVariable("id") Integer id) {
        try {
            personService.delete(id);

            return new ResponseEntity<Person>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Person>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PostMapping("/")
    public ResponseEntity add(@RequestBody Person person) {
        try {
            personService.save(person);
            return new ResponseEntity<Person>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Person>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @PutMapping("/")
    public ResponseEntity<Person> update(@RequestBody Person person) {
        try {
            personService.update(person);
            return new ResponseEntity<Person>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Person>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }


    @GetMapping("/quantityByGender/{gender}")
    @ResponseBody
    public ResponseEntity<Integer> getQuantityByGender(@PathVariable("gender") String gender) {
        try {
            return new ResponseEntity<Integer>(personService.getQuantityByGender(gender),  HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/quantityByAge/{initialAge}/{finalAge}")
    @ResponseBody
    public ResponseEntity<Integer> getQuantityByAge(
            @PathVariable("initialAge") Integer initialAge,
            @PathVariable("finalAge") Integer finalAge){
        try {
            return new ResponseEntity<Integer>(personService.getQuantityByAge(initialAge, finalAge),  HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<Integer>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

